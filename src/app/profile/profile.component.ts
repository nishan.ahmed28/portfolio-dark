import { Component } from '@angular/core';
import * as AOS from 'aos';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {

  ngOnInit(): void{
      AOS.init();
   
  }

  slides = [
    {img: "assets/angular.svg"},
    {img: "assets/cloud-solid.svg"},
    {img: "assets/git.svg"},
    {img: "assets/html5.svg"},
    {img: "assets/js.svg"},
    {img: "assets/node.svg"},
    {img: "assets/sass.svg"},
  ];
  slideConfig = {"slidesToShow": 4, 
    "slidesToScroll": 4,
    "autoplay": true,
    "autuplaySpeed": 5000,
    "pauseOnHover": true,
    "infinite": true,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa-solid fa-arrow-left" style="color: #ffffff;"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa-solid fa-arrow-right" style="color: #ffffff;"></i></button>',
    "responsive": [
      {
        breakpoint: 1200,
        settings: {
          arrows: true,
          infinite: true,
          slidesToShow: 4,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 992,
        settings: {
          arrows: true,
          infinite: true,
          slidesToShow: 4,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: true,
          infinite: true,
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  
  
  
  };
  // ngAfterViewInit() {
  //   this.inlineSVGs();
  // }

  // inlineSVGs() {
  //   document.querySelectorAll('.slide img').forEach((img) => {
  //     fetch(img.getAttribute('src')!)
  //       .then(response => response.text())
  //       .then(svg => {
  //         const svgElement = new DOMParser().parseFromString(svg, 'image/svg+xml').querySelector('svg')!;
  //         svgElement.setAttribute('fill', '#C0C0C0');
  //         svgElement.setAttribute('width', '100px');
  //         svgElement.setAttribute('height', '100px');
  //         img.replaceWith(svgElement);
  //       });
  //   });
  // }


}
