import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-flip-card',
  templateUrl: './flip-card.component.html',
  styleUrls: ['./flip-card.component.css']
})
export class FlipCardComponent implements OnInit {
  cards = [
    {
      frontTitle: 'NOW',
      frontSubtitle: 'Jr.Programmer',
      frontText: 'MYSOFT LTD',
      backHead: '2022 - Current',
      backSub: 'Jr.Programmer',
      backComp: 'MYSOFT LTD',
      backText: 'I have joined MYSOFT LTD as a Trainee Programmer on 16th July 2022. Currently I am working as Jr.Programmer here.'
      
    },
    {
      frontTitle: '2021-2022',
      frontSubtitle: 'Digital Marketing Executive',
      frontText: 'Next Door UK',
      backHead: '2021-2022',
      backSub: 'Digital Marketing Executive',
      backComp: 'Next Door UK',
      backText: 'Right after my graduation I was curious about Digital Marketing sector, Thus I joined Next Door UK a startup E-commerce Company where I learned and worked with Digital Marketting.'
      
    },
    {
      frontTitle: '2021',
      frontSubtitle: 'Intern',
      frontText: 'GIGA TECH Limited',
      backHead: '2021',
      backSub: 'Intern',
      backComp: 'GIGA TECH Limited',
      backText: 'I have done my Internship from GIGA TECH LTD. During My Internship period I worked as a Data Annotator here.'
    },
    // Add more cards as needed
  ];

  ngOnInit() {
    this.adjustCardHeights();
  }

  adjustCardHeights() {
    setTimeout(() => {
      const fronts = document.getElementsByClassName('front');
      const backs = document.getElementsByClassName('back');
  
      let highest = 0;
      let absoluteSide = '';
  
      for (let i = 0; i < fronts.length; i++) {
        if (fronts[i].clientHeight > backs[i].clientHeight) {
          if (fronts[i].clientHeight > highest) {
            highest = fronts[i].clientHeight;
            absoluteSide = '.front';
          }
        } else if (backs[i].clientHeight > highest) {
          highest = backs[i].clientHeight;
          absoluteSide = '.back';
        }
      }
  
      document.querySelectorAll('.front').forEach((element: any) => {
        element.style.height = `${highest}px`;
      });
      document.querySelectorAll('.back').forEach((element: any) => {
        element.style.height = `${highest}px`;
      });
  
      const element = document.querySelector(absoluteSide);
      if (element) {
        // element.setAttribute('style', 'position: absolute');
      }
    }, 0);
  }

}
